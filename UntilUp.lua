local UU = CreateFrame("Frame")
UU:RegisterEvent("PLAYER_XP_UPDATE")
UU:RegisterEvent("UNIT_PET_EXPERIENCE")
UU:RegisterEvent("PLAYER_ENTERING_WORLD")
UU:RegisterEvent("UNIT_PET")

UU:SetScript("OnEvent", function(self, event, unit)
  if event == "PLAYER_ENTERING_WORLD" then
    UU.playerOldXP =  UnitXP("player")
  end

  if event == "UNIT_PET" and unit == "player" then
    UU.petOldXP, _ = GetPetExperience()
  end

  if event == "PLAYER_XP_UPDATE" then
    UU.playerNewXP = UnitXP("player")
    UU.playerMaxXP = UnitXPMax("player")

    if UU.playerOldXP ~= nil and UU.playerNewXP > UU.playerOldXP then
      UU.playerObtainedXP = UU.playerNewXP - UU.playerOldXP
      UU.playerResult = floor((UU.playerMaxXP - UU.playerNewXP) / UU.playerObtainedXP)

      if (UU.playerResult < 1) then
        UU.playerResult = 1
      end

      UIErrorsFrame:SetTimeVisible(5)
      UIErrorsFrame:AddMessage("~" .. UU.playerResult .. " until player levelup")
    end

    UU.playerOldXP = UU.playerNewXP
  end

  if event == "UNIT_PET_EXPERIENCE" then
    UU.petNewXP, UU.petMaxXP = GetPetExperience()

    if UU.petOldXP ~= nil and UU.petNewXP > UU.petOldXP then
      UU.petObtainedXP = UU.petNewXP - UU.petOldXP
      UU.petResult = floor((UU.petMaxXP - UU.petNewXP) / UU.petObtainedXP)

      if (UU.petResult < 1) then
        UU.petResult = 1
      end

      UIErrorsFrame:SetTimeVisible(5)
      UIErrorsFrame:AddMessage("~" .. UU.petResult .. " until pet levelup")
    end

    UU.petOldXP = UU.petNewXP
  end
end)